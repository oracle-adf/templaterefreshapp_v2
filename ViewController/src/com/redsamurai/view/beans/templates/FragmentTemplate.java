package com.redsamurai.view.beans.templates;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import oracle.adf.view.rich.component.rich.layout.RichPanelStretchLayout;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.context.AdfFacesContext;


public class FragmentTemplate {
    private RichPanelStretchLayout fragmentTemplateLayout;

    public FragmentTemplate() {
        super();
    }

    public void setFragmentTemplateLayout(RichPanelStretchLayout fragmentTemplateLayout) {
        this.fragmentTemplateLayout = fragmentTemplateLayout;
    }

    public RichPanelStretchLayout getFragmentTemplateLayout() {
        return fragmentTemplateLayout;
    }

    public void refreshPageTemplate(ActionEvent actionEvent) {
        UIComponent uicomp = this.getFragmentTemplateLayout().getParent();
        while (uicomp != null) {
            if (uicomp.getFamily().equals("org.apache.myfaces.trinidad.ShowOne")) {
                break;
            }
            uicomp = uicomp.getParent();
        }
        if (uicomp != null) {
            List<UIComponent> tabs = uicomp.getChildren();
            for (int i = 0; i < tabs.size(); i++) {
                RichShowDetailItem tab = (RichShowDetailItem)tabs.get(i);
                tab.setDisclosed(!tab.isDisclosed());
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(uicomp);
        }
    }
}
